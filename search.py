# search.py
# ---------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html
from pacman import GameState
from mhlib import PATH
from StdSuites.Type_Names_Suite import null
from twisted.protocols.amp import Path
from os import path
from zope.interface.tests.test_adapter import IF0

"""
In search.py, you will implement generic search algorithms which are called 
by Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
  """
  This class outlines the structure of a search problem, but doesn't implement
  any of the methods (in object-oriented terminology: an abstract class).
  
  You do not need to change anything in this class, ever.
  """
  
  def getStartState(self):
     """
     Returns the start state for the search problem 
     """
     util.raiseNotDefined()
    
  def isGoalState(self, state):
     """
       state: Search state
    
     Returns True if and only if the state is a valid goal state
     """
     util.raiseNotDefined()

  def getSuccessors(self, state):
     """
       state: Search state
     
     For a given state, this should return a list of triples, 
     (successor, action, stepCost), where 'successor' is a 
     successor to the current state, 'action' is the action
     required to get there, and 'stepCost' is the incremental 
     cost of expanding to that successor
     """
     util.raiseNotDefined()

  def getCostOfActions(self, actions):
     """
      actions: A list of actions to take
 
     This method returns the total cost of a particular sequence of actions.  The sequence must
     be composed of legal moves
     """
     util.raiseNotDefined()
           

def tinyMazeSearch(problem):
  """
  Returns a sequence of moves that solves tinyMaze.  For any other
  maze, the sequence of moves will be incorrect, so only use this for tinyMaze
  """
  from game import Directions
  s = Directions.SOUTH
  w = Directions.WEST
  return  [s,s,w,s,w,w,s,w]

def depthFirstSearch(problem):
  """
  Search the deepest nodes in the search tree first [p 85].
  
  Your search algorithm needs to return a list of actions that reaches
  the goal.  Make sure to implement a graph search algorithm [Fig. 3.7].
  
  To get started, you might want to try some of these simple commands to
  understand the search problem that is being passed in:
  
  print "Start:", problem.getStartState()
  print "Is the start a goal?", problem.isGoalState(problem.getStartState())
  print "Start's successors:", problem.getSuccessors(problem.getStartState())
  """
  "*** YOUR CODE HERE ***"
  
#   print "Start:", problem.getStartState()
#   print problem.getCostOfActions([s,s,w,s,w,w,s,w])
#   print problem.getSuccessors(problem.getStartState())
#   print "Is the start a goal?", problem.isGoalState((1,1))
  
  from util import Stack
  
  # Create stack
  frontier = Stack()
  explored = []
     
  # Push first node into stack (coord, path)
  frontier.push((problem.getStartState(), []))
     
  while not frontier.isEmpty() :
      # Get node to work on
      current, path = frontier.pop()
      
      # Return Path if goal state
      if problem.isGoalState(current) :
          return path
      
      for succ, direction, cost in problem.getSuccessors(current) :
          
          # Check if explored
          if succ not in explored :
              
              # Add current to explored
              explored.append(current)
              
              # Push into stack if not explored
              frontier.push((succ, path + [direction]))
   
  return []


def breadthFirstSearch(problem):
  "Search the shallowest nodes in the search tree first. [p 81]"
  "*** YOUR CODE HERE ***"
  
  from util import Queue
  
  # Create queue
  frontier = Queue()
  explored = []
     
  # Push first node into queue (coord, path)
  frontier.push((problem.getStartState(), []))

     
  while not frontier.isEmpty() :
      # Get node to work on
      current, path = frontier.pop()
      
      # Return Path if goal state
      if problem.isGoalState(current) :
          return path
      
      for succ, direction, cost in problem.getSuccessors(current) :
          
          # Check if explored
          if succ not in explored :
              # Add current to explored
              explored.append(succ)
                           
              # Push into queue if not explored
              frontier.push((succ, path + [direction]))
              

   
  return []

      
def uniformCostSearch(problem):
  "Search the node of least total cost first. "
  "*** YOUR CODE HERE ***"
  from util import PriorityQueue
  
  # Create PriorityQueue
  frontier = PriorityQueue()
  explored = []
     
  # Push first node into PriorityQueue (coord, path), cost
  frontier.push((problem.getStartState(), []), 0)
     
  while not frontier.isEmpty() :
      # Get node to work on
      current, path = frontier.pop()
      
      # Return Path if goal state
      if problem.isGoalState(current) :
          return path
      
      for succ, direction, cost in problem.getSuccessors(current) :
          
          # Check if explored
          if succ not in explored :
              
              # Add current to explored
              explored.append(succ)
              
              # Push into PriorityQueue if not explored
              frontier.push((succ, path + [direction]), problem.getCostOfActions(path + [direction]))
              
  return []


def nullHeuristic(state, problem=None):
  """
  A heuristic function estimates the cost from the current state to the nearest
  goal in the provided SearchProblem.  This heuristic is trivial.
  """
  return 0

def aStarSearch(problem, heuristic=nullHeuristic):
  "Search the node that has the lowest combined cost and heuristic first."
  "*** YOUR CODE HERE ***"
  
  from util import PriorityQueue
  
  # Create PriorityQueue
  frontier = PriorityQueue()
  explored = []
     
  # Push first node into PriorityQueue (coord, path), cost
  frontier.push((problem.getStartState(), []), 0)
     
  while not frontier.isEmpty() :
      # Get node to work on
      current, path = frontier.pop()
      
      # Return Path if goal state
      if problem.isGoalState(current) :
          return path
      
      for succ, direction, cost in problem.getSuccessors(current) :
          
          # Check if explored
          if succ not in explored :
              # Add current to explored
              explored.append(succ)
              
              # Get cost so far + heuristic cost
              heuristicAndCost = problem.getCostOfActions(path + [direction]) + heuristic(succ, problem)
              
              # Push into PriorityQueue if not explored
              frontier.push((succ, path + [direction]), heuristicAndCost)
              
  return []

    
  
# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch